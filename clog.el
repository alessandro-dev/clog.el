;;; clog.el --- Clog implementation on Emacs -*- lexical-binding: t; -*-
;;
;; Copyright (C) 2021 Alessandro Martini
;;
;; Author: Alessandro Martini <https://gitlab.com/alessandro-dev>
;; Maintainer: Alessandro Martini <https://gitlab.com/alessandro-dev>
;; Created: July 27, 2021
;; Modified: July 28, 2021
;; Version: 0.0.1
;; Keywords: convenience tools
;; Homepage: https://gitlab.com/alessandro-dev/clog.el
;; Package-Requires: ((emacs "25.1") magit forge yaml-mode projectile)
;;
;; This file is not part of GNU Emacs.
;;
;;; Commentary:
;;
;;  Clog implementation on Emacs
;;
;;  TODO: allow user to specify completing read function. this will allow the user to use ivy/helm/etc...
;;  TODO: validate clog--entry on the constructor.
;;  TODO: write unit tests.
;;  TODO: replace projectile find root with `f--traverse-upwards' (see https://github.com/scottaj/mocha.el/blob/6a72fa20e7be6e55c09b1bc9887ee09c5df28e45/mocha.el#L135)
;;
;;; Code:

(require 'cl-lib)
(require 'magit)
(require 'forge)
(require 'yaml-mode)
(require 'projectile)
(require 'f)

(cl-defstruct clog--entry
  "Struct for the clog data entry."
  description type pr-number)

(cl-defmethod clog--entry-encode ((e clog--entry))
  "Encode clog-entry E into yaml string."

  (let ((data (make-hash-table)))
    (puthash "description" (clog--entry-description e) data)
    (puthash "type" (clog--entry-type e) data)
    (puthash "pr_number" (clog--entry-pr-number e) data)
    (format "\n---%s\n" (yaml-encode data))))

(defgroup clog nil
  "Clog implementation on Emacs."
  :group 'tools)

(defcustom clog-type-choices
  '("added" "changed" "removed" "fixed" "depecrated" "security")
  "List of valid types for the clog entries."
  :group 'clog
  :type '(list string))

(defcustom clog-auto-commit 'ask
  "Specifies auto commit behaviour."
  :group 'clog
  :type '(radio (const :tag 'Yes' 'yes)
                (const :tag 'No' 'no)
                (const :tag 'Ask' 'ask)))

(defcustom clog-auto-commit-message "chore(changelog): add changelog entry"
  "Commit message for `clog-auto-commit'."
  :group 'clog
  :type '(string))

(defun clog--sluggify (str)
  "Convert STR into a safe string to be used as the file base name."
  (downcase (replace-regexp-in-string
             "[^a-z0-9-]" "" (replace-regexp-in-string "\/" "-" str))))

(defun clog--write (entry changelog-file)
  "Write ENTRY to CHANGELOG-FILE."
  (write-region
   (clog--entry-encode entry) nil changelog-file 'append))

(defun clog--auto-commit (changelog-file)
  "Automatically commit change to CHANGELOG-FILE."
  (magit-call-git "add" changelog-file)
  (magit-call-git "commit" "-m" clog-auto-commit-message)
  (magit-refresh))

(defun clog--commit (changelog-file)
  "Commit change to CHANGELOG-FILE based on `clog-auto-commit'."
  (pcase clog-auto-commit
    ('yes (clog--auto-commit changelog-file))
    ('ask (when (y-or-n-p "Commit automatically? ")
            (clog--auto-commit changelog-file)))))

(defun clog--changelog-dir ()
  "Return directory of the changelog unreleased entries."
  (let ((changelog-dir (expand-file-name "changelogs/unreleased" (projectile-project-root))))
    (unless (f-exists? changelog-dir)
      (user-error "Changelog directory does not exist"))
    changelog-dir))

(defun clog--changelog-file ()
  "Return the path to the changelog file for the current entry."
  (expand-file-name (concat (clog--sluggify (or (magit-get-current-branch) "")) ".yml")
                    (clog--changelog-dir)))

(defun clog--validate-entry (entry)
  "Validate ENTRY."
  (when (string-blank-p (clog--entry-description entry))
    (user-error "Missing description"))
  (unless (member (clog--entry-type entry) clog-type-choices)
    (user-error "Invalid type"))
  (when (string-blank-p (clog--entry-pr-number entry))
    (user-error "Missing PR number")))

;;;###autoload
(defun clog ()
  "Create a changelog entry based on the clog pattern."
  (interactive)
  (let* ((changelog-file (clog--changelog-file))
         (description (read-string "Description: "))
         (type (completing-read "Type: " clog-type-choices))
         (pr-number (number-to-string (forge-read-pullreq "PR" t)))
         (entry (make-clog--entry :description description
                                  :type type
                                  :pr-number pr-number)))
    (clog--validate-entry entry)
    (clog--write entry changelog-file)
    (clog--commit changelog-file)))

(provide 'clog)
;;; clog.el ends here
